pipeline {
    agent any
    environment {
      TAG_VERSION = 1.0
      USER = '8cox'
      IMG = 'jala-compiler-web'
      IMG_BUILD = 'jala-compiler-web-build'
    }
    stages {
        stage('Build') {
            steps {
                sh 'docker build -t ${USER}/${IMG}:${TAG_VERSION} .'
            }
        }
        stage('Tests') {
            steps {
                sh 'docker container run ${USER}/${IMG}:${TAG_VERSION} python manage.py test test/core &> test.log'
            }
            post {
                always {
                    archiveArtifacts artifacts: 'test.log', fingerprint: true
                }
            }
        }
        stage('CodeQuality') {
            steps {
                script {
                    withCredentials([string(credentialsId: 'sonar-token', variable: 'SONAR_TOKEN')]){
                        sh  '''
                            export PATH="$PATH:/opt/sonar-scanner/bin" &&
                            sonar-scanner -v &&
                            sonar-scanner \
                                -Dsonar.organization=andres-cox \
                                -Dsonar.projectKey=andres-cox_ac_jalacompiler_web_fork \
                                -Dsonar.sources=. \
                                -Dsonar.host.url=https://sonarcloud.io \
                                -Dsonar.login=$SONAR_TOKEN
                            ''' 
                    }
                }
            }
        }
        stage('Package') {
            steps {
                sh 'export DOCKER_BUILDKIT=1 && docker build -t ${USER}/${IMG_BUILD}:${TAG_VERSION} -f build-Dockerfile .'
            }
        }
        stage('Publish') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'ac-dockerhub', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]){
                        sh 'docker login -u="$DOCKER_USERNAME" -p="$DOCKER_PASSWORD"'
                        sh 'docker push ${USER}/${IMG_BUILD}:${TAG_VERSION}'
                    }
                }
            }      
        }
        stage('DeployToTestAcceptance') {
            steps {
                sh 'docker-compose up -d'
            }  
        }
        stage('Acceptance') {
            steps {
                sh 'echo acceptance test commands run here'
            }  
        }
    }
}