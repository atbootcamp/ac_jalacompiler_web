FROM 8cox/jalacompiler-base:latest
ENV PYTHONUNBUFFERED=1
RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN python -m pip install -r requirements.txt \
    && mkdir ./third_parties \
    && cd ./third_parties \
    && mkdir ./python ./java ./javascript ./php \
    && ln -s /usr/local/bin/python3.9 ./python/Python39-32 \
    && ln -s /usr/bin/python2.7 ./python/Python27 \
    && ln -s /usr/bin/nodejs ./javascript/nodejs14.15.3 
